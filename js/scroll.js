document.onclick = function(event) {
        var target = event.target;

        while (target.tagName != "A") {
                target = target.parentNode;
                if (target == null) return;
        };

        var elemScrollTo = document.querySelector(target.getAttribute("href"));
        elemScrollTo.scrollIntoView({block: "start", behavior: "smooth"});
        return false;
};
